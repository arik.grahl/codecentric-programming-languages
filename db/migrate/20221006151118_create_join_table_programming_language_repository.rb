class CreateJoinTableProgrammingLanguageRepository < ActiveRecord::Migration[7.0]
  def change
    create_join_table :programming_languages, :repositories do |t|
      t.index [:programming_language_id, :repository_id], name: 'index_prog_langs_repos_on_prog_lang_id_and_repo_id'
      t.index [:repository_id, :programming_language_id], name: 'index_prog_langs_repos_on_repo_id_and_prog_lang_id'
    end
    add_foreign_key :programming_languages_repositories, :programming_languages
    add_foreign_key :programming_languages_repositories, :repositories
  end
end

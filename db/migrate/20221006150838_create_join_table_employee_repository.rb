class CreateJoinTableEmployeeRepository < ActiveRecord::Migration[7.0]
  def change
    create_join_table :employees, :repositories do |t|
      t.index [:employee_id, :repository_id]
      t.index [:repository_id, :employee_id]
    end
    add_foreign_key :employees_repositories, :employees
    add_foreign_key :employees_repositories, :repositories
  end
end

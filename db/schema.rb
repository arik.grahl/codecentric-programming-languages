# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2022_10_06_151118) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "employees", force: :cascade do |t|
    t.string "login"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "employees_repositories", id: false, force: :cascade do |t|
    t.bigint "employee_id", null: false
    t.bigint "repository_id", null: false
    t.index ["employee_id", "repository_id"], name: "index_employees_repositories_on_employee_id_and_repository_id"
    t.index ["repository_id", "employee_id"], name: "index_employees_repositories_on_repository_id_and_employee_id"
  end

  create_table "programming_languages", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "programming_languages_repositories", id: false, force: :cascade do |t|
    t.bigint "programming_language_id", null: false
    t.bigint "repository_id", null: false
    t.index ["programming_language_id", "repository_id"], name: "index_prog_langs_repos_on_prog_lang_id_and_repo_id"
    t.index ["repository_id", "programming_language_id"], name: "index_prog_langs_repos_on_repo_id_and_prog_lang_id"
  end

  create_table "repositories", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "employees_repositories", "employees"
  add_foreign_key "employees_repositories", "repositories"
  add_foreign_key "programming_languages_repositories", "programming_languages"
  add_foreign_key "programming_languages_repositories", "repositories"
end

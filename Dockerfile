FROM ruby:3.1.2-alpine

RUN \
	apk update && \
	apk upgrade && \
	apk add \
		build-base \
		libc-dev \
		libffi-dev \
		libgcrypt-dev \
		libxml2-dev \
		libxslt-dev \
		postgresql-dev \
		tzdata \
	&& \
	rm -rfv /var/cache/apk/*

WORKDIR /usr/src/app

COPY Gemfile Gemfile.lock /usr/src/app/
RUN bundle check || bundle install

COPY . /usr/src/app

CMD ["bin/webapp"]

class EmployeesController < ApplicationController
  def index
    @programming_languages = ProgrammingLanguage.all
    params[:programming_languages] ||= []

    @employees = Employee.select do |employee|
      (employee.programming_languages.keys & params[:programming_languages]).sort == params[:programming_languages].sort
    end

    respond_to do |format|
      format.html {}
      format.json {}
      format.yaml do
        render inline: (@employees.each_with_object({}) do |employee, employees|
                         employees[employee[:login]] = employee.programming_languages
                       end).to_yaml
      end
    end
  end
end

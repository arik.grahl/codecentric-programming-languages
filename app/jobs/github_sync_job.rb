class GithubSyncJob < ApplicationJob
  queue_as :default

  BASE_URL = 'https://api.github.com'
  OWNER = :codecentric
  PER_PAGE = 100
  GITHUB_TOKEN = ENV.fetch('GITHUB_TOKEN')
  HEADERS = {
    Authorization: "Bearer #{GITHUB_TOKEN}"
  }.freeze

  def perform(*args)
    /.*page=(?<last_repository_page>[0-9])+/ =~ HTTParty.head("#{BASE_URL}/orgs/#{OWNER}/repos?per_page=#{PER_PAGE}", headers: HEADERS)['link']

    (1..last_repository_page.to_i).each do |repository_page|
      HTTParty.get("#{BASE_URL}/orgs/#{OWNER}/repos?per_page=#{PER_PAGE}&page=#{repository_page}", headers: HEADERS)
              .parsed_response
              &.each do |repo|
                programming_languages = []
                HTTParty.get("#{BASE_URL}/repos/#{OWNER}/#{repo['name']}/languages", headers: HEADERS)
                        .parsed_response
                        &.each do |language, _loc|
                          programming_languages << ProgrammingLanguage.find_or_create_by(name: language)
                        end

                contributors = []
                HTTParty.get("#{BASE_URL}/repos/#{OWNER}/#{repo['name']}/contributors?per_page=#{PER_PAGE}", headers: HEADERS)
                        .parsed_response
                        &.each do |contributor|
                          contributors << Employee.find_or_create_by(employee_mapping(contributor)) rescue raise "#{BASE_URL}/repos/#{OWNER}/#{repo['name']}/contributors?per_page=#{PER_PAGE}".to_s
                        end

                repository = Repository.find_or_create_by(repository_mapping(repo))
                repository.update(programming_languages:, contributors:)
              end
    end
  end

  private

  def employee_mapping(employee)
    employee.select { |key, _value| %w[id login].include?(key) }
  end

  def repository_mapping(repository)
    repository.select { |key, _value| %w[id name].include?(key) }
  end
end

class Repository < ApplicationRecord
  has_and_belongs_to_many :contributors, class_name: 'Employee'
  has_and_belongs_to_many :programming_languages

  validates :name, presence: true
end

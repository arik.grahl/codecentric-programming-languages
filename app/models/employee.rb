class Employee < ApplicationRecord
  has_and_belongs_to_many :repositories

  validates :login, presence: true

  def programming_languages
    self.repositories
        .map(&:programming_languages)
        .flatten
        .inject(Hash.new(0)) do |total, element|
          total[element.name] += 1
          total
        end
  end
end

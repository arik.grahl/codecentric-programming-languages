class ProgrammingLanguage < ApplicationRecord
  has_and_belongs_to_many :repositories

  validates :name, presence: true
end

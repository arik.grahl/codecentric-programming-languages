#!/usr/bin/env ruby
# bin/add-n-to-m-reference.rb

require 'rails'

begin
  n_entity = ARGV[0]
  m_entity= ARGV[1]
  n_entity, m_entity = [n_entity, m_entity].sort
  n_entities = n_entity.pluralize
  m_entities = m_entity.pluralize
  raise unless n_entity && m_entity
rescue
  puts "usage: ./#{$0} N_ENTITY M_ENTITY"
  exit 1
end

timestamp = Time.now.strftime('%Y%m%d%H%M%S')

File.open("db/migrate/#{timestamp}_create_join_table_#{n_entity}_#{m_entity}.rb", 'w') do |file|
  file.write("class CreateJoinTable#{n_entity.camelize}#{m_entity.camelize} < ActiveRecord::Migration[7.0]\n")
  file.write("  def change\n")
  file.write("    create_join_table :#{n_entities}, :#{m_entities} do |t|\n")
  file.write("      t.index [:#{n_entity}_id, :#{m_entity}_id]\n")
  file.write("      t.index [:#{m_entity}_id, :#{n_entity}_id]\n")
  file.write("    end\n")
  file.write("    add_foreign_key :#{n_entities}_#{m_entities}, :#{n_entities}\n")
  file.write("    add_foreign_key :#{n_entities}_#{m_entities}, :#{m_entities}\n")
  file.write("  end\n")
  file.write("end")
end

Rails.application.routes.draw do
  resources :programming_languages, only: [:index]
  resources :repositories, only: [:index]
  resources :employees, only: [:index]
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  root "employees#index"
end

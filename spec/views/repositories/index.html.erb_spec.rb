require 'rails_helper'

RSpec.describe "repositories/index", type: :view do
  before(:each) do
    assign(:repositories, [
      Repository.create!(
        :name => "Name"
      ),
      Repository.create!(
        :name => "Name"
      )
    ])
  end

  it "renders a list of repositories" do
    render
    assert_select "aside>h2", count: 2
  end
end

require 'rails_helper'

RSpec.describe "programming_languages/index", type: :view do
  before(:each) do
    assign(:programming_languages, [
      ProgrammingLanguage.create!(
        :name => "Name"
      ),
      ProgrammingLanguage.create!(
        :name => "Name"
      )
    ])
  end

  it "renders a list of programming_languages" do
    render
    assert_select "aside>h2", count: 2
  end
end

require 'rails_helper'

RSpec.describe "employees/index", type: :view do
  before(:each) do
    assign(:employees, [
      Employee.create!(
        :login => "Login"
      ),
      Employee.create!(
        :login => "Login"
      )
    ])
  end

  it "renders a list of employees" do
    render
    assert_select "aside>h2", count: 2
  end
end

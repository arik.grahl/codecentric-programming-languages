require "rails_helper"

RSpec.describe RepositoriesController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/repositories").to route_to("repositories#index")
    end
  end
end

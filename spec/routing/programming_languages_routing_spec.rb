require "rails_helper"

RSpec.describe ProgrammingLanguagesController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/programming_languages").to route_to("programming_languages#index")
    end
  end
end

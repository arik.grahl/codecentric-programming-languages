require 'rails_helper'

RSpec.describe ProgrammingLanguage, type: :model do
  it { should have_and_belong_to_many(:repositories) }
  it { should validate_presence_of(:name) }
end

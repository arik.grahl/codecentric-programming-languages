require 'rails_helper'

RSpec.describe Employee, type: :model do
  it { should have_and_belong_to_many(:repositories) }
  it { should validate_presence_of(:login) }
end

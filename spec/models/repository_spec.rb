require 'rails_helper'

RSpec.describe Repository, type: :model do
  it { should have_and_belong_to_many(:contributors).class_name('Employee') }
  it { should have_and_belong_to_many(:programming_languages) }
  it { should validate_presence_of(:name) }
end

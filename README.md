# Codecentric Programming Languages

## Building

### Podman

The webapplication can be built with the `Dockerfile` in the project's directory.

```
podman build \
	-t codecentric-programming-languages \
	.
```

### Nix

The following nix shell provides necessary system requirements.

```
nix-shell -p ruby_3_1 postgresql
```

In this nix shell the bundle can be installed by invoking

```
bundle install
```

## Running

A postgres endpoint is a requirement for the webapplication.

### Podman

The webapp, the GitHub synchronization job and a database can be started using the predefined k8s manifests.

```
podman play kube k8s.yml
```

### Nix

The postgres endpoint should be exposed as an environment variable.

```
export DATABASE_HOST=localhost
```

The webapp can be started with the corresponding shell script.

```
bin/webapp
```

The GitHub synchronization job needs an `GITHUB_TOKEN` which should be exposed as an environment variable.

```
export GITHUB_TOKEN=ghp_1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ
```

Afterwards the synchronization job can be executed by using the particular shell script.

```
bin/github-sync
```

## Using

### HTTP endpoint

A minimalistic YAML-view of employees and their respective language is available as an HTTP endpoint.
The `programming_languages` can be specified as query parameter.
An example usage for querying for `Scala` and `Shell` could look like the following:

```
curl -s 'http://localhost:3000/employees.yaml?programming_languages[]=Scala&programming_languages[]=Shell'
```

The output is something like

```yaml
---
# ...
ftrossbach:
  JavaScript: 1
  HTML: 2
  Shell: 1
  Scala: 1
rbraeunlich:
  Scala: 1
  Shell: 1
```

### Webapp

A convenient webapplication is available under http://localhost:3000 which can be consumed in common webbrowsers.

```
xdg-open http://localhost:3000
```

## Security

Common vulnerabilities can be scanned with [`trivy`](https://github.com/aquasecurity/trivy).

### Local bundle

```
trivy fs .
```

### Container

```
trivy image codecentric-programming-languages
```
